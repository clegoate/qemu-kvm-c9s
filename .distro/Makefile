include Makefile.common

REDHAT:=$(shell pwd)
RPMBUILD:=$(REDHAT)/rpmbuild
SCRIPTS:=$(REDHAT)/scripts
KOJI_OPTIONS:=$(KOJI_FLAGS) --scratch

# Options section

# Hide command calls without debug option
ifeq ($(DEBUG),1)
  DS=
else
  DS=@
endif

# Hide progress bar in scripts
ifeq ($(NOPROGRESS),1)
  KOJI_OPTIONS:=$(KOJI_OPTIONS) --noprogress
endif

# Do not wait for build finish
ifeq ($(NOWAIT),1)
  KOJI_OPTIONS:=$(KOJI_OPTIONS) --nowait
endif


# create an empty localversion file if you don't want a local buildid
ifneq ($(NOLOCALVERSION),1)
  ifeq ($(LOCALVERSION),)
    LOCALVERSION:=$(shell cat localversion 2>/dev/null)
  endif
  ifeq ($(LOCALVERSION),)
    LOCALVERSION:=$(shell id -u -n)$(shell date +"%Y%m%d%H%M")
  endif
else
  LOCALVERSION:=
endif


.PHONY: rh-clean-sources rh-prep rh-srpm rh-rhel-koji rh-centos-koji rh-help rh-srpm-gcc rh-rhel-koji-gcc rh-centos-koji-gcc rh-env-prep rh-env-prep-gcc rh-local rh-local-gcc
all: rh-help

rh-clean-sources:
	$(DS)for i in $(RPMBUILD)/SOURCES/*; do \
		rm -f $$i; \
	done;

rh-prep: rh-clean-sources
	$(DS)if [ ! -f $(TARFILE) ]; then \
		wget $(TARURL); \
		if [ -n "$(TARSHA512)" -a -e $(REDHAT)/$(TARFILE) -a "`$(SCRIPTS)/tarball_checksum.sh $(TARFILE)`" != "$(TARSHA512)" ]; then \
			echo "$(TARFILE) sha512sum does not match (expected: $(TARSHA512) / got `$(SCRIPTS)/tarball_checksum.sh $(TARFILE)`)"; \
			exit 1; \
		fi; \
	fi; 
	$(DS)if [ -n "$(SOURCES_FILELIST)" ]; then \
		echo "Copying Sources: $(SOURCES_FILELIST)"; \
		cp $(SOURCES_FILELIST) $(RPMBUILD)/SOURCES; \
	fi
	$(DS)$(SCRIPTS)/process_patches.sh "$(CLANG)" "$(TARFILE)" "$(SPECFILE)" "$(MARKER)" "$(LOCALVERSION)"

rh-srpm: rh-prep
	$(DS)rpmbuild --define "_sourcedir $(RPMBUILD)/SOURCES" --define "_builddir $(RPMBUILD)/BUILD" --define "_srcrpmdir $(RPMBUILD)/SRPMS" --define "_rpmdir $(RPMBUILD)/RPMS" --define "_specdir $(RPMBUILD)/SPECS" --define "dist $(DIST)" --nodeps -bs $(RPMBUILD)/SPECS/$(SPECFILE)

rh-srpm-gcc: CLANG=0
rh-srpm-gcc: rh-srpm

rh-rhel-koji: rh-srpm
	@echo "Build $(SRPM_NAME) as $(BUILD_TARGET_RHEL)"
	$(DS)brew build $(KOJI_OPTIONS) $(BUILD_TARGET_RHEL) $(RPMBUILD)/SRPMS/$(SRPM_NAME)

rh-rhel-koji-gcc: CLANG=0
rh-rhel-koji-gcc: rh-rhel-koji

rh-centos-koji: rh-srpm
	@echo "Build $(SRPM_NAME) as $(BUILD_TARGET_C9S)"
	$(DS)koji --profile=stream build $(KOJI_OPTIONS) $(BUILD_TARGET_C9S) $(RPMBUILD)/SRPMS/$(SRPM_NAME)

rh-centos-koji-gcc: CLANG=0
rh-centos-koji-gcc: rh-centos-koji

rh-env-prep: rh-prep
	$(DS)sudo dnf install -y wget rpm-build
	$(DS)sudo dnf builddep $(RPMBUILD)/SPECS/$(SPECFILE) || echo "Build dependency installation FAILED."
	$(DS)mkdir -p $(REDHAT)/../build
	$(DS)cd $(REDHAT)/../build; $(shell rpmspec -P $(RPMBUILD)/SPECS/$(SPECFILE) | $(SCRIPTS)/extract_build_cmd.py | sed "s/--with-git-submodules=ignore/--with-git-submodules=update/g")

rh-local: rh-env-prep
	$(MAKE) -C $(REDHAT)/../build

rh-env-prep-gcc: CLANG=0
rh-env-prep-gcc: rh-env-prep

rh-local-gcc: CLANG=0
rh-local-gcc: rh-local

rh-help:
	@echo "Supported make targets:"
	@echo "  rh-srpm:            Create srpm"
	@echo "  rh-rhel-koji:       Build package using RHEL 9 koji and Clang compiler"
	@echo "  rh-rhel-koji-gcc:   Build package using RHEL 9 koji and GCC compiler"
	@echo "  rh-centos-koji:     Build package using CentOS 9 Streams koji and Clang compiler"
	@echo "  rh-centos-koji-gcc: Build package using CentOS 9 Streams koji and GCC compiler"
	@echo "rh-env-prep:          Install rpms required for redhat build and run configure (using Clang compiler)"
	@echo "rh-env-prep-gcc:      Install rpms required for redhat build and run configure (using GCC compiler)"
	@echo "rh-local:             Run local (non-rpm) build (using Clang compiler). Runs rh-env-prep on each call."
	@echo "rh-local-gcc:         Run local (non-rpm) build (using GCC compiler). Runs rh-env-prep on each call."
	@echo "  rh-help:            Print out help"
	@echo ""
	@echo "Supported variables:"
	@echo "  NOPROGRESS:     If 1, do not show progressbar when uploading srpm"
	@echo "  NOWAIT:         If 1, do not wait for build to finish"
	@echo "  NOLOCALVESION:  If 1, do append LOCALVERSION to release version"
	@echo "  LOCALVERSION:   Use value as release version suffix"
	@echo""
	@echo "LOCALVERSION can be stored in .distro/localversion file."
	@echo "If LOCALVERSION is not set and there's no (or empty) .distro/localversion file,"
	@echo "date +"%Y%m%d%H%M" is used instead."
	@echo ""
	@echo "Note: Running rh-local can fail in case new rpms are installed. Please re-run it for correc build."


